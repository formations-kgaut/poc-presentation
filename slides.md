# Présentation Reveal.js
## Gitlab-CI + Gitlab Pages


--

# Pour plus d'informations

Et quelques explications : https://kgaut.net/blog/2022/deployer-ses-presentations-sur-gitlab-pages-avec-revealjs-docker-et-gitlab-ci.html

---

# Comment ça marche ?
1. Un projet template qui intègre reveal js dans une image docker : https://gitlab.com/formations-kgaut/template-reveal-js
2. Un sous projet, comme celui là, qui contient les slides et utilise l'image docker précédement crée
3. La CI de ce projet publie la présentation automatiquement à chaque modification sur gitlab page.

--

# Intéret ?
- Rédaction de présentation très rapide
- Historique des modifications
- Fork pour réutiliser / faire une nouvelle version de présentation

