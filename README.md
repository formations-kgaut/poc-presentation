# POC Présentation

Présentation « Proof of concept » de mon système de présentation

Plus d'informations et d'explications : https://kgaut.net/blog/2022/deployer-ses-presentations-sur-gitlab-pages-avec-revealjs-docker-et-gitlab-ci.html

Présentation visible ici : https://formations-kgaut.gitlab.io/poc-presentation

Projet template : https://gitlab.com/formations-kgaut/template-reveal-js
